/*
Error Boundaries does not catch error in
1. server side rendering
2. error thrown in error boundary itself rather than its children
3. event handlers 
4. Async code (setTimeout or requestAnimationFrame Callback) 

a class component becomes error boundaries if it has either or both static getDerivedStateFromError(), componentDidCatch()

these are component those catches error anywhere in the child component tree , 
it catches error in the rendering in lifezcycle method ,  and in constructor of the whole tree below them.

*/
import logo from "./logo.svg";
import "./App.css";
import Display from "./components/Display";
import Error from "./components/Error";

function App() {
  return (
    <div>
      <Error>
        <Display name={"swapnil"} roll={101} />
      </Error>
      <Error>
        <Display name={"Rakesh"} roll={103} />
      </Error>
      <Error>
        <Display name={102} roll={105} />
      </Error>
    </div>
  );
}

export default App;
