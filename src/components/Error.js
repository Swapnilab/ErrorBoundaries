import React, { Component } from "react";

export default class Error extends Component {
  state = {
    hasError: false,
  };
  static getDerivedStateFromError(error) {
    console.log("error thrown", error);
    return { hasError: true };
  }
  componentDidCatch(error, info) {
    console.log("error  in did catch ", error);
    console.log("info in did catch", info);
  }
  render() {
    if (this.state.hasError) {
      return <h3>Error! Something went wrong</h3>;
    } else return <div>{this.props.children}</div>;
  }
}
