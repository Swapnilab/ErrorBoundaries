import React from "react";

function Display({ name, roll }) {
  if (typeof name !== "string") {
    throw new Error("Error ! Incorrect Name Entered");
  } else
    return (
      <div>
        Hello {name} Your Roll No is {roll}
      </div>
    );
}

export default Display;
